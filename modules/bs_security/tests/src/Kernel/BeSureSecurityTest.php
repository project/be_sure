<?php

namespace Drupal\Tests\bs_security\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test BeSure Security module.
 *
 * @group bs_security
 */
class BsSecurityTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'be_sure',
    'bs_security',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['system']);
  }

  /**
   * Test when modules disabled.
   *
   * @dataProvider getModulesListData
   */
  public function testDisabledModules($module, $callback, $expected) {
    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists($module)) {
      $this->disableModules([$module]);
    }
    $this->assertEquals(call_user_func($callback), !$expected);
  }

  /**
   * Test when modules enabled.
   *
   * @dataProvider getModulesListData
   */
  public function testEnabledModules($module, $callback, $expected) {
    $this->enableModules([$module]);
    $this->assertEquals(call_user_func($callback), $expected);
  }

  /**
   * Provide data for testDisabledModules and testEnabledModules().
   *
   * @return array
   *   Dummy HTML pages for checking.
   */
  public function getModulesListData() {
    return [
      ['seckit', 'bs_security_seckit', TRUE],
      ['username_enumeration_prevention', 'bs_security_username_enumeration_prevention', TRUE],
      ['password_policy', 'bs_security_password_policy', TRUE],
      ['codefilter', 'bs_security_code_filter', FALSE],
      ['php', 'bs_security_php_filter', FALSE],
      ['syslog', 'bs_security_syslog', TRUE],
    ];
  }

  /**
   * Cover bs_security_visible_errors().
   *
   * @dataProvider getErrorLevelValuesData
   */
  public function testVisibleErrors($error_level, $expected) {
    $config = \Drupal::service('config.factory')->getEditable('system.logging');
    $config->set('error_level', $error_level);
    $config->save();

    $this->assertSame($expected, bs_security_visible_errors());
  }

  /**
   * Provide data for testVisibleErrors().
   *
   * @return array
   */
  public function getErrorLevelValuesData() {
    return [
      ['hide', TRUE],
      ['some', FALSE],
      ['all', FALSE],
      ['verbose', FALSE],
    ];
  }

}
