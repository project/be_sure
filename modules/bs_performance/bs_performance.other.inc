<?php

/**
 * @file
 * Other part of performance.
 */

/**
 * Check last cron ran.
 */
function bs_performance_other_last_cron() {
  $request_time = \Drupal::time()->getRequestTime();
  return \Drupal::state()->get('system.cron_last') > ($request_time - 86400);
}

/**
 * Check availability of CSS optimization.
 */
function bs_performance_other_css() {
  return (bool) \Drupal::config('system.performance')->get('css.preprocess');
}

/**
 * Check availability of JavaScript optimization.
 */
function bs_performance_other_js() {
  return (bool) \Drupal::config('system.performance')->get('js.preprocess');
}

/**
 * Check PHP execution time.
 */
function bs_performance_other_max_execution() {
  return ini_get('max_execution_time') < 300;
}

/**
 * Check availability of Advanced CSS/JS Aggregation.
 */
function bs_performance_other_advagg() {
  return \Drupal::moduleHandler()->moduleExists('advagg')
    && \Drupal::moduleHandler()->moduleExists('advagg_css_minify')
    && \Drupal::moduleHandler()->moduleExists('advagg_js_minify');
}
