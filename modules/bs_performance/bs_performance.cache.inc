<?php

/**
 * @file
 * Cache part of performance.
 */

/**
 * Check availability of Page caching.
 */
function bs_performance_cache_caching() {
  return \Drupal::moduleHandler()->moduleExists('page_cache');
}

/**
 * Check availability of Varnish.
 */
function bs_performance_cache_dynamic_caching() {
  return \Drupal::moduleHandler()->moduleExists('dynamic_page_cache');
}

/**
 * Check cache max age.
 */
function bs_performance_cache_max_age() {
  return \Drupal::config('system.performance')->get('cache.page.max_age') >= 300;
}

/**
 * Check availability of Memcache or Redis.
 */
function bs_performance_cache_memcache_redis() {
  return \Drupal::moduleHandler()->moduleExists('memcache')
  || \Drupal::moduleHandler()->moduleExists('redis')
  || \Drupal::moduleHandler()->moduleExists('memcache_storage');
}
