<?php

namespace Drupal\Tests\bs_performance\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test cache cases for be_sure.
 *
 * @group bs_performance
 */
class BeSurePerformanceModuleTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'be_sure',
    'bs_performance',
  ];

  /**
   * Cover bs_performance_cache_*() functions.
   *
   * @dataProvider getModuleCasesData
   */
  public function testModuleCases($module, $callback, $expected) {
    if ($module) {
      $this->enableModules([$module]);
    }
    $status = $callback();

    $this->assertSame($expected, $status);
  }

  /**
   * Provide data for testModuleCases().
   *
   * @return array
   */
  public function getModuleCasesData() {
    return [
      ['fast404', 'bs_performance_module_fast_404', TRUE],
      [NULL, 'bs_performance_module_fast_404', FALSE],
      ['image_resize_filter', 'bs_performance_module_image_resize_filter', TRUE],
      [NULL, 'bs_performance_module_image_resize_filter', FALSE],
      ['imageapi_optimize', 'bs_performance_module_imageapi_optimize', TRUE],
      [NULL, 'bs_performance_module_imageapi_optimize', FALSE],
      ['backup_migrate', 'bs_performance_module_backup', FALSE],
      [NULL, 'bs_performance_module_backup', TRUE],
      ['update', 'bs_performance_module_update', FALSE],
      [NULL, 'bs_performance_module_update', TRUE],
      ['dblog', 'bs_performance_module_dblog', FALSE],
      [NULL, 'bs_performance_module_dblog', TRUE],
    ];
  }

}
