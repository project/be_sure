<?php

namespace Drupal\Tests\bs_performance\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test cache cases for be_sure.
 *
 * @group bs_performance
 */
class BeSurePerformanceCacheTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'be_sure',
    'bs_performance',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['system']);
  }

  /**
   * Cover bs_performance_cache_memcache_redis().
   *
   * @dataProvider getCacheCasesData
   */
  public function testCacheCases($module, $callback, $expected) {
    if ($module) {
      $this->enableModules([$module]);
    }
    $status = $callback();

    $this->assertSame($expected, $status);
  }

  /**
   * Provide data for testCacheCases().
   *
   * @return array
   */
  public function getCacheCasesData() {
    return [
      ['memcache', 'bs_performance_cache_memcache_redis', TRUE],
      ['redis', 'bs_performance_cache_memcache_redis', TRUE],
      ['memcache_storage', 'bs_performance_cache_memcache_redis', TRUE],
      [NULL, 'bs_performance_cache_memcache_redis', FALSE],
      ['dynamic_page_cache', 'bs_performance_cache_dynamic_caching', TRUE],
      [NULL, 'bs_performance_cache_dynamic_caching', FALSE],
      ['page_cache', 'bs_performance_cache_caching', TRUE],
      [NULL, 'bs_performance_cache_caching', FALSE],
    ];
  }

  /**
   * Cover bs_performance_cache_max_age().
   *
   * @dataProvider getCacheMaxAgeData
   */
  public function testCacheMaxAge($age, $expected) {
    $config = \Drupal::service('config.factory')->getEditable('system.performance');
    $config->set('cache.page.max_age', $age);
    $config->save();
    $status = bs_performance_cache_max_age();

    $this->assertSame($expected, $status);
  }

  /**
   * Provide data for testCacheMaxAge().
   *
   * @return array
   */
  public function getCacheMaxAgeData() {
    return [
      [0, FALSE],
      [180, FALSE],
      [300, TRUE],
      [900, TRUE],
    ];
  }

}
