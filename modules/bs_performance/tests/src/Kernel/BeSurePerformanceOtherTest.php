<?php

namespace Drupal\Tests\bs_performance\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test cache cases for be_sure.
 *
 * @group bs_performance
 */
class BeSurePerformanceOtherTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'be_sure',
    'bs_performance',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->installConfig(['system']);
  }

  /**
   * Cover bs_performance_other_advagg().
   *
   * @dataProvider getAdvaggCaseData
   */
  public function testAdvaggCase($modules, $callback, $expected) {
    if ($modules) {
      $this->enableModules($modules);
    }
    $status = $callback();

    $this->assertSame($expected, $status);
  }

  /**
   * Cover bs_performance_other_js() and bs_performance_other_css().
   *
   * @dataProvider getCssJsAggregateData
   */
  public function testCssJsAggregate($config_name, $value, $expected) {
    $config = \Drupal::service('config.factory')->getEditable('system.performance');
    $config->set("$config_name.preprocess", $value);
    $config->save();
    $callback = 'bs_performance_other_' . $config_name;
    $status = $callback();

    $this->assertSame($expected, $status);
  }

  /**
   * Cover bs_performance_other_last_cron().
   *
   * @dataProvider getLastCronData
   */
  public function testLastCron($value, $expected) {
    \Drupal::state()->set('system.cron_last', time() - $value);
    $this->assertSame($expected, bs_performance_other_last_cron());
  }

  /**
   * Provide data for testLastCron().
   *
   * @return array
   */
  public function getLastCronData() {
    return [
      [7, TRUE],
      [84400, TRUE],
      [99400, FALSE],
      [87400, FALSE],
    ];
  }

  /**
   * Provide data for testCssJsAggregate().
   *
   * @return array
   */
  function getCssJsAggregateData() {
    return [
      ['js', 1, TRUE],
      ['js', 0, FALSE],
      ['css', 1, TRUE],
      ['css', 0, FALSE],
    ];
  }

  /**
   * Provide data for testAdvaggCase().
   *
   * @return array
   */
  public function getAdvaggCaseData() {
    return [
      [['advagg', 'advagg_css_minify', 'advagg_js_minify'], 'bs_performance_other_advagg', TRUE],
      [['advagg', 'advagg_js_minify'], 'bs_performance_other_advagg', FALSE],
      [['advagg', 'advagg_css_minify'], 'bs_performance_other_advagg', FALSE],
      [['advagg'], 'bs_performance_other_advagg', FALSE],
      [['advagg_css_minify'], 'bs_performance_other_advagg', FALSE],
      [['advagg_js_minify'], 'bs_performance_other_advagg', FALSE],
      [[], 'bs_performance_other_advagg', FALSE],
    ];
  }

}
