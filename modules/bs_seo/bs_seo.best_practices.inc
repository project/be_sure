<?php

/**
 * @file
 * Best practices part of SEO.
 */

/**
 * Check availability of Search 404.
 */
function bs_seo_best_practices_search404() {
  return Drupal::moduleHandler()->moduleExists('search404');
}

/**
 * Check availability of Google Analytics.
 */
function bs_seo_best_practices_gag() {
  return Drupal::moduleHandler()->moduleExists('google_analytics')
  && mb_strlen(Drupal::config('google_analytics.settings')->get('account')) > 3;
}

/**
 * Check availability of cache Google analytics.
 */
function bs_seo_best_practices_gag_cache() {
  return bs_seo_best_practices_gag()
  && Drupal::config('google_analytics.settings')->get('cache');
}

/**
 * Check availability of Site Verification.
 */
function bs_seo_best_practices_site_verify() {
  return Drupal::moduleHandler()->moduleExists('site_verify');
}
