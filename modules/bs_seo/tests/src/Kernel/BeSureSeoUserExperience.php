<?php

namespace Drupal\Tests\bs_seo\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test basic Bs_seo UserExperience.
 *
 * @group bs_seo
 */
class BeSureSeoUserExperience extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'be_sure',
    'bs_seo',
  ];

  /**
   * Test bs_seo_user_experience_redirect() without Redirect.
   */
  public function testStatusSeoUserExperienceRedirect() {
    $status = bs_seo_user_experience_redirect();
    $this->assertFalse($status, 'Module "Redirect" should not be installed');
  }

  /**
   * Test bs_seo_user_experience_redirect() with Redirect.
   */
  public function testEnabledSeoUserExperienceRedirect() {
    $this->enableModules(['redirect']);
    $status = bs_seo_user_experience_redirect();
    $this->assertTrue($status, 'Module "Redirect" should be installed');
  }

  /**
   * Test bs_seo_user_experience_scheduler() without Scheduler.
   */
  public function testStatusSeoUserExperienceScheduler() {
    $status = bs_seo_user_experience_scheduler();
    $this->assertFalse($status, 'Module "Scheduler" should not be installed');
  }

  /**
   * Test bs_seo_user_experience_scheduler() with Scheduler.
   */
  public function testEnabledSeoUserExperienceScheduler() {
    $this->enableModules(['scheduler']);
    $status = bs_seo_user_experience_scheduler();
    $this->assertTrue($status, 'Module "Scheduler" should be installed');
  }

  /**
   * Test bs_seo_user_experience_site_map() without Sitemap.
   */
  public function testStatusSeoUserExperienceSitemap() {
    $status = bs_seo_user_experience_site_map();
    $this->assertFalse($status, 'Module "Sitemap" should not be installed');
  }

  /**
   * Test bs_seo_user_experience_site_map() with Sitemap.
   */
  public function testEnabledSeoUserExperienceSitemap() {
    $this->enableModules(['sitemap']);
    $status = bs_seo_user_experience_site_map();
    $this->assertTrue($status, 'Module "Sitemap" should be installed');
  }

}
