<?php

namespace Drupal\Tests\be_sure\modules\bs_seo\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test basic Bs_seo Crawlability.
 *
 * @group bs_seo
 */
class BeSureSeoCrawlability extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'be_sure',
    'bs_seo',
  ];

  /**
   * Test bs_seo_crawlability_xmlsitemap() without XMLSitemap.
   */
  public function testStatusSeoCrawlabilityXMLSitemap() {
    $status = bs_seo_crawlability_xmlsitemap();
    $this->assertFalse($status, 'Module "XML Sitemap" should not be installed');
  }

  /**
   * Test bs_seo_crawlability_xmlsitemap() with XMLSitemap.
   */
  public function testEnabledSeoCrawlabilityXMLSitemap() {
    $this->enableModules(['xmlsitemap']);
    $status = bs_seo_crawlability_xmlsitemap();
    $this->assertTrue($status, 'Module "XML Sitemap" should be installed');
  }

  /**
   * Test bs_seo_crawlability_robotstxt() without RobotsTxt.
   */
  public function testStatusCrawlabilityRobotstxt() {
    $status = bs_seo_crawlability_robotstxt();
    $this->assertFalse($status, 'Module "robotsTxt" should not be installed');
  }

  /**
   * Test bs_seo_crawlability_robotstxt() with RobotsTxt.
   */
  public function testEnabledCrawlabilityRobotstxt() {
    $this->enableModules(['robotstxt']);
    $status = bs_seo_crawlability_robotstxt();
    $this->assertTrue($status, 'Module "robotsTxt" should be installed');
  }

}
