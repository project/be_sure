<?php

namespace Drupal\Tests\be_sure\modules\bs_seo\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test basic Bs_seo BestPractices.
 *
 * @group be_sure
 */
class BeSureSeoBestPractices extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'be_sure',
    'bs_seo',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
  }

  /**
   * Test bs_seo_best_practices_search404() without module Search404.
   */
  public function testStatusSeoBestPracticesSearch404() {
    $status = bs_seo_best_practices_search404();
    $this->assertFalse($status, 'Module "search404" should not be installed');
  }

  /**
   * Test bs_seo_best_practices_search404() with module Search404.
   */
  public function testEnabledSeoBestPracticesSearch404() {
    $this->enableModules(['search404']);
    $status = bs_seo_best_practices_search404();
    $this->assertTrue($status, 'Module "search404" should be installed');
  }

  /**
   * Test bs_seo_best_practices_site_verify() without module Site Verify.
   */
  public function testStatusSeoBestPracticesSiteVerify() {
    $status = bs_seo_best_practices_site_verify();
    $this->assertFalse($status, 'Module "Site Verify" should not be installed');
  }

  /**
   * Test bs_seo_best_practices_site_verify() with module Site Verify.
   */
  public function testEnabledSeoBestPracticesSiteVerify() {
    $this->enableModules(['site_verify']);
    $status = bs_seo_best_practices_site_verify();
    $this->assertTrue($status, 'Module "Site Verify" should be installed');
  }

  /**
   * Test bs_seo_best_practices_gag() without module Google Analytics.
   */
  public function testStatusSeoBestPracticesGag() {
    $status = bs_seo_best_practices_gag();
    $this->assertFalse($status, 'Module "Google Analytics" should not be installed');
  }

  /**
   * Test bs_seo_best_practices_gag() with module Google Analytics.
   */
  public function testEnabledSeoBestPracticesGag() {
    $this->enableModules(['google_analytics']);
    $this->installConfig(['google_analytics']);
    $status = bs_seo_best_practices_gag();
    $this->assertTrue($status, 'Web Property ID not filled');
  }

  /**
   * Test bs_seo_best_practices_gag_cache() without caching code.
   */
  public function testStatusSeoBestPracticesGagCache() {
    $status = bs_seo_best_practices_gag_cache();
    $this->assertFalse($status, 'Code should not be cached');
  }

}
