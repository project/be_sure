<?php

namespace Drupal\Tests\be_sure\modules\bs_seo\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test basic Bs_seo PageStructure.
 *
 * @group be_sure
 */
class BeSureSeoPageStructure extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'be_sure',
    'bs_seo',
  ];

  /**
   * Test bs_seo_page_structure_pathauto() without pathauto.
   */
  public function testStatusSeoPageStructurePathauto() {
    $status = bs_seo_page_structure_pathauto();
    $this->assertFalse($status, 'Module "pathauto" should not be installed');
  }

  /**
   * Test bs_seo_page_structure_pathauto() with pathauto.
   */
  public function testEnabledSeoPageStructurePathauto() {
    $this->enableModules(['pathauto', 'token']);
    $status = bs_seo_page_structure_pathauto();
    $this->assertTrue($status, 'Module "pathauto" should be installed');
  }

  /**
   * Test bs_seo_page_structure_metatag() without metatag.
   */
  public function testStatusSeoPageStructureMetatag() {
    $status = bs_seo_page_structure_metatag();
    $this->assertFalse($status, 'Module "metatag" should not be installed');
  }

  /**
   * Test bs_seo_page_structure_metatag() with metatag.
   */
  public function testEnabledSeoPageStructureMetatag() {
    $this->enableModules(['metatag']);
    $status = bs_seo_page_structure_metatag();
    $this->assertTrue($status, 'Module "metatag" should be installed');
  }

}
