<?php

namespace Drupal\bs_git\Controller;

use Drupal\Core\Link;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Url;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class BeSureGitController.
 *
 * @package Drupal\bs_git\Controller
 */
class BeSureGitController extends ControllerBase {
  use StringTranslationTrait;

  /**
   * Files_status.
   *
   * @return string
   *   Return Hello string.
   */
  public function filesStatus() {
    $info = _bs_git_get_status();
    $items = [];

    if (!$info) {
      $output = '<div class="messages status">';
      $output .= $this->t('There are no changed files.');
      $output .= '</div>';
    }
    else {
      $output[] = [
        '#markup' => '<div class="messages error">' .
        $this->t('There are changed files.') . '</div>',
      ];

      foreach ($info as $file) {
        $file = explode(' ', trim($file));
        $file = array_values(array_filter($file));

        list($status, $file_path) = $file;

        $status = mb_strlen($status) > 1
          ? mb_substr($status, -1, 1)
          : $status;

        $items[$status][] = $status == BS_GIT_FILE_CHANGED
          ? $file_path . ' ' . Link::fromTextAndUrl(
            $this->t('(show diff)'),
            Url::fromRoute('bs_git.file_diff', [],
              ['query' => ['file' => $file_path]]
            )
          )
          : $file_path;
      }

      $titles = [
        BS_GIT_FILE_ADDED => $this->t('Added'),
        BS_GIT_FILE_NEW => $this->t('Created'),
        BS_GIT_FILE_CHANGED => $this->t('Modified'),
        BS_GIT_FILE_DELETED => $this->t('Deleted'),
      ];

      foreach ($items as $status => $value) {
        $title = $titles[$status];

        $output[] = [
          '#theme' => 'item_list',
          '#items' => $value,
          '#title' => $title,
        ];
      }
    }

    return $output;
  }

  /**
   * File_diff.
   *
   * @return string
   *   Return Hello string.
   */
  public function fileDiff() {
    $file = isset($_GET['file']) ? (string) $_GET['file'] : '';

    if (!$file || !file_exists($file)) {
      $output = Response::HTTP_NOT_FOUND;
    }
    else {
      $git_command = bs_git_get_command();
      exec(escapeshellcmd($git_command) . ' diff ' . escapeshellarg($file), $diff, $retval);

      $output = '<pre>';
      $output .= implode(PHP_EOL, $diff);
      $output .= '</pre>';
    }

    return $output;
  }

}
