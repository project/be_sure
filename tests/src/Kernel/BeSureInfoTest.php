<?php

namespace Drupal\Tests\be_sure\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test basic BeSure info.
 *
 * @group be_sure
 */
class BeSureInfoTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'be_sure',
  ];

  /**
   * Test be_sure_get_info() without submodules.
   */
  public function testEmptySureInfo() {
    $info = be_sure_get_info();
    $this->assertEmpty($info, 'Without submodules, info (be_sure_get_info()) should be empty.');
  }

  /**
   * Test be_sure_get_info() within at least one submodule.
   */
  public function testSureInfo() {
    $this->enableModules(['bs_performance']);

    $info = be_sure_get_info();
    $this->assertNotEmpty($info, 'Within submodules, info (be_sure_get_info()) should not be empty.');
  }

}
