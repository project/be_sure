CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Be Sure is designed to help developers and webmasters monitor their Drupal
sites using a suite of administrative dashboards. This module provides three
different dashboards focusing on SEO, performance, and security configurations.
 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/be_sure


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

This module contains various sub-modules to customize the dashboards:

 * Performance
    Performance part checking common cache settings (front-end and back-end).
    Checking status of "good for performance" modules, such as "Views Litepager"
    and other. Also, it checking settings for css/js optimization and some PHP
    settings.

 * Security
    Check common security issues, such as common super-admin username and
    availability of some "good for security" modules.

 * File status
    Checks the status of files using git to be sure about integrity of your
    files.

 * SEO
    The SEO sub-module checks the issues and shows a categorized list of modules
    that are not enabled.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will display the dashboards under Administration > Reports
> Be sure.


MAINTAINERS
-----------

Current maintainer:
 * Dmitry Kiselev (kala4ek) - https://www.drupal.org/u/kala4ek
